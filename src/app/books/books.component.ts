import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

import { BookService } from './../shared/services/book.service';
import { BookResponse } from './../shared/services/book.schema';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {

  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = [
    'id',
    'cover',
    'attributes.content',
    'type',
    'attributes.created_at',
    'attributes.updated_at'
  ];

  constructor(
    private service: BookService
  ) { }

  ngOnInit() {
    // table sorting and filter
    this.dataSource.filterPredicate = this.generalFilterPredicate;
    this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
    this.dataSource.sort = this.sort;

    // get books list
    this.getBooks();
  }

  getBooks() {
    this.service.getBooks().subscribe(
      (response: BookResponse) => {
        this.dataSource.data = response.data;
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // This function access nested object sorting.
  private sortingDataAccessor(item, property) {
    if (property.includes('.')) {
      return property.split('.')
        .reduce((object, key) => object[key], item);
    }
    return item[property];
  }

  // This function is allow for nested object search.
  // I made the json object to string and then search with the string value.
  private generalFilterPredicate(data, filter: string) {
    const dataStr = JSON.stringify(data).toLowerCase();
    return dataStr.indexOf(filter) !== -1;
  }

}
