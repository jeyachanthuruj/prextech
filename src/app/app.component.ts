import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';

import { fadeInOutAnimation } from './app-routing-animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    fadeInOutAnimation
  ]
})
export class AppComponent implements OnInit {
  title = 'PerxTech';
  showRoutes = false;

  constructor(private router: Router) {}

  ngOnInit() {
    this.toggleMenu();
  }

  toggleMenu() {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.showRoutes = (event.url !== '/welcome' && event.url !== '/');
      }
    });
  }
}
