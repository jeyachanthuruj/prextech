interface ILinks {
    self: string;
    related?: string;
}

interface IReleationship {
    links: ILinks;
}

export class Book {
    id: string; // id mentioned as a string in given sample data json.
    type: string;
    links: ILinks;
    attributes: {
        urn: string;
        created_at: string;
        updated_at: string;
        content: string;
        properties?: string;
        display_properties: {
            type: string;
            image: string;
        };
    };
    relationships: {
        [key: string]: IReleationship;
    };
}

export interface BookResponse {
    data: Book[];
    links: {
        first: string;
        last: string;
    };
}
