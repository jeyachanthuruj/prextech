import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appTextReplace]'
})
export class TextReplaceDirective {

  oldText: string;
  @Input('appTextReplace') newText: string;

  @HostListener('mouseenter') onMouseEnter() {
    // store old content
    if (!this.oldText) {
      this.oldText = this.el.nativeElement.innerText;
    }

    this.highlight('#fff');
    this.el.nativeElement.textContent = this.newText;
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null);
    this.el.nativeElement.textContent = this.oldText;
  }

  constructor(private el: ElementRef) { }

  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }

}
