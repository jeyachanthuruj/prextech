import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UseOfDirectivesComponent } from './use-of-directives.component';

describe('UseOfDirectivesComponent', () => {
  let component: UseOfDirectivesComponent;
  let fixture: ComponentFixture<UseOfDirectivesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UseOfDirectivesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UseOfDirectivesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
