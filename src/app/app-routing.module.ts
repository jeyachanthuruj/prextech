import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeComponent } from './welcome/welcome.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BooksComponent } from './books/books.component';
import { UseOfDirectivesComponent } from './use-of-directives/use-of-directives.component';

const routes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: 'books', component: BooksComponent },
  { path: 'use-of-directives', component: UseOfDirectivesComponent },
  { path: '', redirectTo: '/welcome', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
