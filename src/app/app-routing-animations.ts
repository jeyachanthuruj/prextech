import {
    trigger,
    transition,
    style,
    query,
    animateChild,
    animate,
    group
} from '@angular/animations';

export const fadeInOutAnimation =
    trigger('routeAnimations', [
        transition('* <=> *', [
            style({ position: 'relative' }),
            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    opacity: 0,
                    width: '100%'
                })
            ], { optional: true }),
            query(':enter', [
                style({ opacity: 0 })
            ], { optional: true }),
            query(':leave', [
                style({ opacity: 1 })
            ], { optional: true }),
            group([
                query(':leave', [
                    animate('1000ms ease-out', style({ opacity: 0 }))
                ], { optional: true }),
                query(':enter', [
                    animate('1000ms ease-out', style({ opacity: 1 }))
                ], { optional: true })
            ]),
            query(':enter', animateChild(), { optional: true }),
        ])
    ]);
